

class Queue {
    constructor(){
        this.items = [];
    }
}

let collection = [];




// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return(collection);
}


//Adds an element/elements to the end of the queue
function enqueue(element) {
   this.items.push(element)
}


//Removes an element in front of the queue
function dequeue(){
    // return this.collection.shift();
    if(this.isEmpty())
        return "Underflow";
    return this.items.shift();
}


//Shows the element at the front
function front(){
    // return this.collection[0]
    if(this.isEmpty()
        return this.items[0])

}

//Shows the total number of elements
function size(){
    return this.collection.length


}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){

}




// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};